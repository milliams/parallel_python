{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 2: Parallel Python\n",
    "\n",
    "Python has many libraries available to help you parallelise your scripts across the cores of a single multicore computer. The traditional option is the [`multiprocessing`](https://docs.python.org/3/library/multiprocessing.html) library but since Python 3.2 (2011) the [`concurrent.futures`](https://docs.python.org/3/library/concurrent.futures.html) module has provided a consistent interface to many ways of running code in parallel."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can find all the details of how to use these features in Python in the [the official documentation page for `concurrent.futures`](https://docs.python.org/3/library/concurrent.futures.html) and some under-the-hood details in [the `multiprocessing` documentation](https://docs.python.org/3/library/multiprocessing.html).\n",
    "\n",
    "One of the first thing to understand is what your computer hardware is capable of, primarily, how many CPU cores it has. Python provides a function in the `os` module called [`cpu_count`](https://docs.python.org/3/library/os.html?#os.cpu_count) which returns the number of CPUs (computer cores) available on your computer to be used for a parallel program:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import os\n",
    "\n",
    "os.cpu_count()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nearly all modern computers have several processor cores, so you should see that you have at least 2, and perhaps as many as 40 available on your machine. Each of these cores is available to do work, in parallel, as part of your Python script. For example, if you have two cores in your computer, then your script should ideally be able to do two things at once. Equally, if you have forty cores available, then your script should ideally be able to do forty things at once.\n",
    "\n",
    "`concurrent.futures` provides a number of ways of running your code in parallel and in this course we will be focussing on its *process pool executor* which allows your script to do lots of things at once by actually running multiple copies of your script in parallel, with (normally) one copy per processor core on your computer. One of these copies is known as the master copy, and is the one that is used to control all of the worker copies. Because of this, Python code has to be written into a text file and executed using the Python interpreter. It is not recommended to try to run a parallel Python script interactively, e.g. via IPython, the Python Console or Jupyter notebooks.\n",
    "\n",
    "In addition, because it achieves parallelism by running multiple copies of your script, it forces you to write it in a particular way. All imports should be at the top of the script, followed by all function and class definitions. This is to ensure that all copies of the script have access to the same modules, functions and classes. Then, you should ensure that only the master copy of the script runs the code by protecting it behind an `if __name__ == \"__main__\"` statement.\n",
    "\n",
    "An example (non-functional) script is shown below:\n",
    "\n",
    "```python\n",
    "# all imports should be at the top of your script\n",
    "import concurrent.futures\n",
    "import os\n",
    "import sys\n",
    "\n",
    "# all function and class definitions must be next\n",
    "def add(x, y):\n",
    "    \"\"\"Function to return the sum of the two arguments\"\"\"\n",
    "    return x + y\n",
    "\n",
    "def product(x, y):\n",
    "    \"\"\"Function to return the product of the two arguments\"\"\"\n",
    "    return x * y\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    # You must now protect the code being run by\n",
    "    # the master copy of the script by placing it\n",
    "    # in this block\n",
    "\n",
    "    a = [1, 2, 3, 4, 5]\n",
    "    b = [6, 7, 8, 9, 10]\n",
    "\n",
    "    # Now write your parallel code...\n",
    "    etc. etc.\n",
    "```\n",
    "\n",
    "(if you are interested, take a look [here](https://chryswoods.com/parallel_python/gil.html) for more information about why parallel Python is based on forking multiple processes, rather than splitting multiple threads)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
