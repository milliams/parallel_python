{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Built-in functions\n",
    "\n",
    "## Operators\n",
    "\n",
    "Throughout the course today we have written several function which perform simple tasks such as:\n",
    "\n",
    "```python\n",
    "def add(x, y):\n",
    "    \"\"\"Function to return the sum of the two arguments\"\"\"\n",
    "    return x + y\n",
    "\n",
    "def product(x, y):\n",
    "    \"\"\"Function to return the product of the two arguments\"\"\"\n",
    "    return x * y\n",
    "\n",
    "def square(x):\n",
    "    \"\"\"Function to return the square of the argument\"\"\"\n",
    "    return x * x\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We had to do this since functional programming requires the passing of the names of real functions. We can't for example do:\n",
    "\n",
    "```python\n",
    "map(*, a, b)\n",
    "```\n",
    "\n",
    "and expect it to multiply together the lists' elements. In our case we instead had to pass in our defined function `product` which takes two arguments:\n",
    "\n",
    "```python\n",
    "map(product, a, b)\n",
    "```\n",
    "\n",
    "Since using mathematical operations in a functional context is very common in Python, it already provides functions which implement the operators in [a module called `operator`](https://docs.python.org/3/library/operator.html):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "35"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import operator\n",
    "\n",
    "operator.mul(5, 7)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which means that we can use them in our `map`s, `ProcessPoolExecutor.map`s and `reduce`s:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[4, 10, 18]"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a = [1, 2, 3]\n",
    "b = [4, 5, 6]\n",
    "\n",
    "list(map(operator.mul, a, b))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[1, 32, 729]"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "list(map(operator.pow, a, b))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These functions can be used in reductions too:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "24"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from functools import reduce\n",
    "\n",
    "reduce(operator.mul, [1, 2, 3, 4])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Take the answer to the first exercise in the [Parallel map/reduce](Map Reduce.ipynb#Exercise) where we created `countlines.py` ([solution here](answer_shakespeare_multi.ipynb)) and rewrite the line\n",
    "\n",
    "```python\n",
    "total = reduce(lambda x, y: x + y, play_line_count)\n",
    "```\n",
    "\n",
    "to use the correct function from the `operator` module.\n",
    "\n",
    "[<small>answer</small>](answer_countlines_operator.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reductions\n",
    "\n",
    "Another common thing we did with our results was to `reduce` them down to a single value by adding them together. We did this with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "10"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "reduce(lambda x, y: x + y, [1, 2, 3, 4])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is such a common thing to do that Python has a built-in function to add together all the numbers in a sequence, `sum`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "10"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sum([1, 2, 3, 4])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The documentation for this function is at [built-in functions](https://docs.python.org/3/library/functions.html) and there are a few other reduction functions such as `min`, `max`, `any` and `all`. Most reduction functions are simple and there's nothing wrong with writing your own but if there's already one provided by Python then it's probably worth using it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Edit `countlines.py` again to use the `sum` function in-place of the custom reduction.\n",
    "\n",
    "[<small>answer</small>](answer_countlines_sum.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Using a reduction function from [the `statistics` module](https://docs.python.org/3/library/statistics.html), edit `countlines.py` to also print the average of the number of lines per file at the end of the program.\n",
    "\n",
    "[<small>answer</small>](answer_countlines_mean.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
