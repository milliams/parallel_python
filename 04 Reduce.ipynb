{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 1: Reduction\n",
    "\n",
    "We have seen how to map a function across a list of data, with the return value of each function call placed into a list of results. For example, you summed together two lists of numbers using `map` using code such as this. In the Python Console type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[7, 9, 11, 13, 15]"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def add(x, y):\n",
    "    \"\"\"Function to return the sum of x and y\"\"\"\n",
    "    return x + y\n",
    "\n",
    "a = [1, 2, 3, 4, 5]\n",
    "b = [6, 7, 8, 9, 10]\n",
    "\n",
    "result = map(add, a, b)\n",
    "\n",
    "list(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This returns a list of results. However, what if we want to sum every value in the returned list of results to form a single value? We could write the code by hand, e.g.:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Total = 55\n"
     ]
    }
   ],
   "source": [
    "total = 0\n",
    "\n",
    "result = map(add, a, b)\n",
    "\n",
    "for i in result:\n",
    "    total += i\n",
    "\n",
    "print(\"Total = %s\" % total)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This process of summing a list of numbers into a total is an example of “reduction”. The list of numbers has been *reduced* to a total by adding each value onto a running total. While mapping takes N elements and creates N new elements, a reduction takes N elements and returns 1 element. Reduction is the complement to mapping, and as such, Python has a [`reduce`](https://docs.python.org/3/library/functools.html#functools.reduce) function.\n",
    "\n",
    "The `reduce` function is available from the standard [`functools`](https://docs.python.org/3/library/functools.html) module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "55"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from functools import reduce\n",
    "\n",
    "result = map(add, a, b)\n",
    "\n",
    "reduce(add, result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`reduce` takes two required arguments and one additional, optional argument:\n",
    "1. The *reduction function* used to reduce a pair of arguments to a single result, e.g. `add` takes two arguments and returns the sum of those arguments. This can be any function that accepts two arguments and returns a single result.\n",
    "2. The list of values to be reduced.\n",
    "3. An (optional) initial value that is used as the first value for the reduction.\n",
    "\n",
    "For example, type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "25"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a = [1, 2, 3, 4, 5]\n",
    "\n",
    "reduce(add, a, 10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Why do you think the answer is `25`?\n",
    "\n",
    "Python’s `reduce` applies the reduction function (in this case `add`) cumulatively from left to right along the items of a list. If an initial value is supplied then this is used as the first value. Otherwise, the first value is the result of the reduction function applied to the first two items in the list. In the above case, reduce performed:\n",
    "1. `total = 10`\n",
    "2. `total = add(total, 1)`\n",
    "3. `total = add(total, 2)`\n",
    "4. `total = add(total, 3)`\n",
    "5. `total = add(total, 4)`\n",
    "6. `total = add(total, 5)`\n",
    "\n",
    "The result is thus `25`, i.e. $(((((10+1)+2)+3)+4)+5)$.\n",
    "\n",
    "The reduction function can be any function that accepts two arguments and returns a single value. For example, let’s now use `reduce` to calculate the product of all of the values in the list. To do this, we need to create a new function that will take in two arguments and return their product:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "120"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def multiply(x, y):\n",
    "    \"\"\"Return the product of the two arguments\"\"\"\n",
    "    return x * y\n",
    "\n",
    "reduce(multiply, a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should see that the product is `120`. Is this what you expected? In this case, `reduce` performed;\n",
    "1. `total = multiply(1, 2)`\n",
    "2. `total = multiply(total, 3)`\n",
    "3. `total = multiply(total, 4)`\n",
    "4. `total = multiply(total, 5)`\n",
    "\n",
    "i.e. it set `total` equal to $((((1×2)×3)×4)×5) = 120$.\n",
    "\n",
    "Note that the reduction function is not limited to just numbers. You can write a reduction function to reduce any types of object together. For example, we could use reduce to join together some strings:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'cat dog mouse fish'"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def join_strings(x, y):\n",
    "    return f\"{x} {y}\"\n",
    "\n",
    "a = [\"cat\", \"dog\", \"mouse\", \"fish\"]\n",
    "\n",
    "reduce(join_strings, a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Modify your `countlines.py` script so that, in addition to printing out the total number of lines in each Shakespeare play, it also uses `reduce` to print out the total number of lines in *all* Shakespeare plays.\n",
    "\n",
    "If you get stuck or want some inspiration, a possible answer is given [here](answer_shakespeare_reduce.ipynb)."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
