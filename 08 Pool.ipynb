{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 2: Pool\n",
    "\n",
    "One of the core `concurrent.futures` features is [`concurrent.futures.ProcessPoolExecutor`](https://docs.python.org/3/library/concurrent.futures.html#processpoolexecutor). This provides a pool of workers that can be used to parallelise a `map`.\n",
    "\n",
    "For example, we have been working with examples like the following which run using the serial map function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 4, 9, 16, 25]\n"
     ]
    }
   ],
   "source": [
    "def square(x):\n",
    "    \"\"\"Function to return the square of the argument\"\"\"\n",
    "    return x * x\n",
    "\n",
    "r = [1, 2, 3, 4, 5]\n",
    "result = map(square, r)\n",
    "print(list(result))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To convert this code to be able to run the function across multiple processors, we need to change it to look like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting pool.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile pool.py\n",
    "\n",
    "from concurrent.futures import ProcessPoolExecutor\n",
    "\n",
    "def square(x):\n",
    "    \"\"\"Function to return the square of the argument\"\"\"\n",
    "    return x * x\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    r = [1, 2, 3, 4, 5]\n",
    "    \n",
    "    # create a pool of workers\n",
    "    with ProcessPoolExecutor() as pool:\n",
    "        result = pool.map(square, r)\n",
    "\n",
    "    print(list(result))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which, when run, gives us the same result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 4, 9, 16, 25]\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python pool.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The core logic of the code has remained the same but we have had to make some changes in order to make it ready to support running in parallel:\n",
    "\n",
    "1. We moved the code that calls the map into an `if __name__ == \"__main__\"` block. This is to ensure that only the master process create the pool of worker processes.\n",
    "2. We have created a pool of workers using `with ProcessPoolExecutor() as pool`. Inside this block we can access the worker pool with the `pool` variable and once we leave the block, the workers will be automatically cleaned up.\n",
    "3. We call `pool.map` instead of just `map` to make the mapping be performed by the workers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parallel work is conducted on the line\n",
    "\n",
    "```python\n",
    "result = pool.map(square, r)\n",
    "```\n",
    "\n",
    "This performs a map of the function `square` over the list of items in `r`. The `map` is divided up over all of the workers in the pool. This means that, if you have 10 workers (e.g. if you have 10 cores), then each worker will perform only one tenth of the work. If you have 2 workers, then each worker will perform only half of the work.\n",
    "\n",
    "You can verify that the `square` function is divided between your workers by using an `os.getpid` call, which will return the process ID (PID) of the worker. We can also manually set the number of worker processes that should be created by passing `max_workers=` to the `ProcessPoolExecutor` constructor. Edit your `pool.py` script and set the contents equal to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting pool.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile pool.py\n",
    "\n",
    "import os\n",
    "from concurrent.futures import ProcessPoolExecutor\n",
    "from functools import reduce\n",
    "\n",
    "def square(x):\n",
    "    \"\"\"Function to return the square of the argument\"\"\"\n",
    "    print(f\"Worker {os.getpid()} calculating square of {x}\")\n",
    "    return x * x\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "    # create a pool of workers\n",
    "    with ProcessPoolExecutor(max_workers=2) as pool:\n",
    "        # create an array of 20 integers, from 1 to 20\n",
    "        r = range(1, 21)\n",
    "\n",
    "        result = pool.map(square, r)\n",
    "\n",
    "    total = reduce(lambda x, y: x + y, result)\n",
    "\n",
    "    print(f\"The sum of the square of the first 20 integers is {total}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run this script using"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Worker 7116 calculating square of 1\n",
      "Worker 7117 calculating square of 2\n",
      "Worker 7116 calculating square of 3\n",
      "Worker 7116 calculating square of 4\n",
      "Worker 7117 calculating square of 5\n",
      "Worker 7117 calculating square of 6\n",
      "Worker 7117 calculating square of 7\n",
      "Worker 7117 calculating square of 8\n",
      "Worker 7117 calculating square of 9\n",
      "Worker 7117 calculating square of 10\n",
      "Worker 7116 calculating square of 11\n",
      "Worker 7117 calculating square of 12\n",
      "Worker 7116 calculating square of 13\n",
      "Worker 7117 calculating square of 14\n",
      "Worker 7116 calculating square of 15\n",
      "Worker 7117 calculating square of 16\n",
      "Worker 7116 calculating square of 17\n",
      "Worker 7116 calculating square of 18\n",
      "Worker 7116 calculating square of 19\n",
      "Worker 7117 calculating square of 20\n",
      "The sum of the square of the first 20 integers is 2870\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python pool.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(the exact PIDs of the workers, and the order in which they print will be different on your machine)\n",
    "\n",
    "You can see in the output that there are two workers, signified by the two different worker PIDs. The work has been divided evenly amongst them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Edit `pool.py` and change the value of `max_workers`. How is the work divided as you change the number of workers?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using multiple pools in a single script\n",
    "\n",
    "You can use more than one `ProcessPoolExecutor` in your script, but you should ensure that you use them one after another. The way `ProcessPoolExecutor` works is to fork your script into the team of workers when you create a `ProcessPoolExecutor` object. Each worker contains a complete copy of all of the functions and variables that exist at the time of the fork. This means that any changes after the fork will not be held by the other workers.\n",
    "\n",
    "If you made a Python script called `broken_pool.py` with the contents:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting broken_pool.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile broken_pool.py\n",
    "\n",
    "from concurrent.futures import ProcessPoolExecutor\n",
    "\n",
    "def square(x):\n",
    "    \"\"\"Return the square of the argument\"\"\"\n",
    "    return x * x\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "\n",
    "    r = [1, 2, 3, 4, 5]\n",
    "\n",
    "    with ProcessPoolExecutor() as pool:\n",
    "        result = pool.map(square, r)\n",
    "\n",
    "        print(f\"Square result: {list(result)}\")\n",
    "\n",
    "        def cube(x):\n",
    "            \"\"\"Return the cube of the argument\"\"\"\n",
    "            return x * x * x\n",
    "\n",
    "        result = pool.map(cube, r)\n",
    "\n",
    "        print(f\"Cube result: {list(result)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and ran it you would see an error like:\n",
    "\n",
    "```\n",
    "AttributeError: Can't get attribute 'cube' on <module '__main__' from 'broken_pool.py'>\n",
    "```\n",
    "\n",
    "The problem is that `pool` was created before the `cube` function. The worker copies of the script were thus created before `cube` was defined, and so don’t contain a copy of this function. This is one of the reasons why you should always define your functions above the `if __name__ == \"__main__\"` block.\n",
    "\n",
    "Alternatively, if you have to define the function in the `__main__` block, then ensure that you create the pool after the definition. For example, one fix here is to create a second pool for the second map:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting pool.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile pool.py\n",
    "\n",
    "from concurrent.futures import ProcessPoolExecutor\n",
    "\n",
    "def square(x):\n",
    "    \"\"\"Return the square of the argument\"\"\"\n",
    "    return x * x\n",
    "\n",
    "if __name__ == \"__main__\":\n",
    "\n",
    "    r = [1, 2, 3, 4, 5]\n",
    "\n",
    "    with ProcessPoolExecutor() as pool:\n",
    "        result = pool.map(square, r)\n",
    "\n",
    "        print(f\"Square result: {list(result)}\")\n",
    "\n",
    "    def cube(x):\n",
    "        \"\"\"Return the cube of the argument\"\"\"\n",
    "        return x * x * x\n",
    "\n",
    "    with ProcessPoolExecutor() as pool:\n",
    "        result = pool.map(cube, r)\n",
    "\n",
    "        print(f\"Cube result: {list(result)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running this should print out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Square result: [1, 4, 9, 16, 25]\n",
      "Cube result: [1, 8, 27, 64, 125]\n"
     ]
    }
   ],
   "source": [
    "!venv/bin/python pool.py"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
